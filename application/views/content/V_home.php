<?php
/******************************************
* Filename    : home.php
* Proggrammer : Robi Naufal Kaosar
* Date        : 2020-04-02
* E-Mail      : robinaufal11@upi.edu
* Deskripsi   : Isi konten halaman beranda
*
******************************************/
if (!is_null($this->session->userdata('kirim_email')) && $this->session->userdata('kirim_email') == 'true') {
  ?>
  <script type="text/javascript">
  alert('Username dan Password anda sudah dikirim melalui E-Mail.');
  </script>
  <?php
  $this->session->sess_destroy();
}
?>

 <div id="slider" class="sl-slider-wrapper" style="margin-top:50px;">

   <div class="sl-slider">
     <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
       <div class="sl-slide-inner">
         <div class="bg-img bg-img-1"></div>
         <?php
            $date_ex = date_create("2020-07-01");
            $date_now = date_create(date("Y-m-d"));
            $diff = date_diff($date_ex, $date_now);
            if($login == 'false'){
              ?>
              <h2>SELAMAT DATANG DI PPDB ONLINE<br>MAN 1 CIANJUR</h2>
              <?php
              if($diff->format("%R%a") < 0){
                ?>
                <blockquote><p>Silahkan klik tombol Daftar untuk melakukan pendaftaran peserta didik baru secara online</p>
                  <a href="<?=base_url('C_daftar')?>" class="bttn-new">Daftar</a>
                </blockquote>
                <?php
              }else{
                ?>
                <blockquote><p>Silahkan klik tombol Login untuk mengakses akun PPDB anda</p>
                  <a href="<?=base_url('C_login')?>" class="bttn-new">Login</a>
                </blockquote>
                <?php
              }
              ?>
              <?php
            }else{
              ?>
              <h2>HAI <?=strtoupper($nama)?></h2>
                <!-- <blockquote><p>Silahkan klik tombol Cetak Kartu untuk mencetak kartu pendaftaran</p>
                  <a onclick="maintenance()" href="<?=base_url('C_cetak_formulir')?>" class="bttn-new">Cetak Kartu</a>
              </blockquote> -->
              <blockquote><p>Silahkan klik tombol Edit Data untuk mengedit dan melihat data pendaftaran anda.</p>
                <a onclick="maintenance()" href="<?=base_url('C_edit_daftar')?>" class="bttn-new">Edit Data</a>
              </blockquote>
              <?php
            }
          ?>
       </div>
     </div>

     <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
       <div class="sl-slide-inner">
         <div class="bg-img bg-img-2"></div>
         <?php
            if ($login == 'false') {
              ?>
              <h2>SELAMAT DATANG DI PPDB ONLINE<br>MAN 1 CIANJUR</h2>
              <blockquote><p>Sekolah Gerbang Depan. </p>
                <?php
                if ($diff->format("%R%a") < 0){
                  ?>
                  <a href="<?=base_url('C_daftar')?>" class="bttn-new">Daftar</a>
                  <?php
                }else{
                  ?>
                  <a href="<?=base_url('C_login')?>" class="bttn-new">Login</a>
                  <?php
                }
                ?>
              </blockquote>
              <?php
            }else{
              ?>
              <h2>HAI <?=strtoupper($nama)?></h2>
              <blockquote><p>Sekolah Gerbang Depan. </p>
                <a onclick="maintenance()" href="<?=base_url('C_edit_daftar')?>" class="bttn-new">Edit Data</a>
              </blockquote>
              <?php
            }
          ?>
       </div>
     </div>

     <!-- <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
       <div class="sl-slide-inner">
         <div class="bg-img bg-img-3"></div>
         <h2>Search engine, Analytics, Traffic</h2>
         <blockquote><p>Etiam felis elit, mollis posuere accumsan ac, dignissim a ligula. Nam ullamcorper ornare tortor sed dapibus. Aliquam ultrices vestibulum sodales. Aenean efficitur massa vel tellus dapibus pellentesque. </p>
           <a href="#" class="bttn-new">Started Today</a>
         </blockquote>
       </div>
     </div>

     <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
       <div class="sl-slide-inner">
         <div class="bg-img bg-img-4"></div>
         <h2>Social Networking</h2>
         <blockquote><p>Etiam felis elit, mollis posuere accumsan ac, dignissim a ligula. Nam ullamcorper ornare tortor sed dapibus. Aliquam ultrices vestibulum sodales. Aenean efficitur massa vel tellus dapibus pellentesque. </p>
           <a href="#" class="bttn-new">Started Today</a>
         </blockquote>
       </div>
     </div> -->
   </div><!-- /sl-slider -->


   <nav id="nav-dots" class="nav-dots">
     <span class="nav-dot-current"></span>
     <span></span>
     <!-- <span></span>
     <span></span> -->
   </nav>

 </div><!-- /slider-wrapper -->

<!-- <section id="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="aligncenter">
          <h2 class="aligncenter">Courses</h2>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident, doloribus omnis minus ovident, doloribus omnis minus temporibus perferendis nesciunt..
        </div>
        <br>
      </div>
    </div>
    <div class="row">
      <div class="skill-home">
        <div class="skill-home-solid clearfix">
          <div class="col-md-3 text-center">
            <span class="icons c1"><i class="fa fa-book"></i></span>
            <div class="box-area">
              <h3>Vocational Courses</h3>
              <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <span class="icons c2"><i class="fa fa-users"></i></span>
            <div class="box-area">
              <h3>MassComm Courses</h3>
              <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <span class="icons c3"><i class="fa fa-trophy"></i></span>
            <div class="box-area">
              <h3>Accounts</h3>
              <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <span class="icons c4"><i class="fa fa-globe"></i></span>
            <div class="box-area">
              <h3>Business Management</h3> <p>Lorem ipsum dolor sitamet, consec tetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->

<div class="container" style="margin-top:25px; background-color:white;">
  <div class="row">
    <div class="col-md-12">
      <div class="aligncenter">
        <h2 class="aligncenter">Informasi Pendaftaran</h2>
      </div>
      <br>
    </div>
  </div>
  <div class="row">
    <div class="col-md-5" style="background-color:yellow;">
      <!-- Heading and para -->
      <div class="block-heading-two">
        <h3><span>Pendaftaran</span></h3>
      </div>
      <table>
           <tr>
          <th>Pendaftaran Online</th>
          <th>:</th>
          <th>&nbsp;ppdb-man-1-cianjur.com</th>
        </tr>
        <tr>
        <tr>
          <th>Pendaftaran Offline</th>
          <th>:</th>
          <th>&nbsp;Seketariat PPDB MAN 1 Cianjur</th>
        </tr>
        <tr>
          <th>Jalur Prestasi</th>
          <th>:</th>
          <th>&nbsp;Ditutup 1 Juli 2020</th>
        </tr>
        <tr>
          <th>Jalur Umum</th>
          <th>:</th>
          <th>&nbsp;Ditutup 1 Juli 2020</th>
        </tr>
        <tr>
          <th>Hari Senin-Jum'at&nbsp;</th>
          <th>:</th>
          <th>&nbsp;Buka jam 08.00 - 14.00 WIB</th>
        </tr>

      </table>
      <br>
      <br>
      <table>
        <tr>
          <th>Kontak Informasi 1&nbsp;&nbsp;</th>
          <th>:</th>
          <th>&nbsp;H.M.Dadang Utsmuni,S.E (083126479406)</th>
        </tr>
        <tr>
          <th>Kontak Informasi 2&nbsp;&nbsp;</th>
          <th>:</th>
          <th>&nbsp;Dandy Idham Chalid,M.Pd (081808962499)</th>
        </tr>
      </table>
    </div>

    <div class="col-md-3" style="background-color:rgb(255, 162, 0);">
      <div class="timetable">
        <h3><span class="fa fa-clock-o"></span> Jadwal Seleksi</h3>
        <hr>
        <h4></h4>
        <dl>
          <dt></dt>
          <dd></dd>
        </dl>
        <hr>
        <h4></h4>
        <dl>
          <dt></dt>
          <dd></dd>
        </dl>
      </div>
    </div>

    <div class="col-md-4" style="background-color:rgb(255, 127, 59);">
      <div class="timetable">
        <h3><span class="fa fa-clock-o"></span> Pengumuman</h3>
        <hr>
        <h4>Jalur Prestasi</h4>
        <dl>
          <dt>Pengumuman Kelulusan(Online/ Offline):</dt>
          <dd>8 Juli 2020</dd>
        </dl>
        <dl>
          <dt>Registrasi ulang dan verifikasi data:</dt>
          <dd>11 Juli 2020</dd>
        </dl>
        <hr>
        <h4>Jalur Umum</h4>
        <dl>
          <dt>Pengumuman Kelulusan(Online/ Offline):</dt>
          <dd>8 Juli 2020</dd>
        </dl>
        <dl>
          <dt>Registrasi ulang dan verifikasi data:</dt>
          <dd>11 Juli 2020</dd>
        </dl>
      </div>
    </div>

  </div>
  <br>
</div>

<div class="container" style="background-color:rgb(28, 104, 117); color:white;">
  <div class="row">
    <div class="col-md-12">
      <div class="text-center">
        <h2 style="color:white;">Ketentuan Kelulusan</h2>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="about-text">
        <ul class="withArrow">
          <span style="font-size:20px">1. Lulusan Satuan Pendidikan SMP/MTs</span>
          <li><span class="fa fa-angle-right"></span> Dibutuhkan dengan adanya Foto Copy Surat Kelulusan yang disahkan oleh satuan pendidikan SMP/MTs.</li>
          <br>
          <span style="font-size:20px">2. Jalur Prestasi</span>
          <li><span class="fa fa-angle-right"></span> Memiliki PrestasiAkademik atau Non Akademik (dibuktikan dengan sertifikat / surat keterangan)  </li>
          <li><span class="fa fa-angle-right"></span> Nilai Raport (SMT.1 - 5) dilampiri scan rapot semester 1 -5 </li>

          <br>
          <span style="font-size:20px">3. Jalur Umum</span>

          <li><span class="fa fa-angle-right"></span>  Nilai Raport (SMT.1 - 5) dilampiri scan rapot semester 1 -5</li>

        </ul>

        <!-- <a href="#" class="btn btn-primary" style="margin-top:25px;margin-bottom:25px;">Pengumuman</a> -->
      </div>
    </div>
    <!-- <div class="col-md-6 col-sm-6">
    <div class="about-image">
    <img src="img/about.jpg" alt="About Images">
  </div>
</div> -->
</div>
</div>
